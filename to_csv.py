def to_csv(fn, ds_type):
    data = json.loads(open(fn, 'r').read())['data']
    lst = []

    for entry in data:
        for paragraph in entry['paragraphs']:
            for qa in paragraph['qas']:
                id = qa['id']
                title = entry['title']
                context = paragraph['context']
                question = qa['question']
                answers = qa['answers'][0]
                ds_type = ds_type
                answer_text = answers['text']
                is_impossible = False

                lst.append((
                    id, title, context, question, answers, ds_type, answer_text, is_impossible
                ))

    return pd.DataFrame(lst, columns=['id', 'title', 'context', 'question', 'answers', 'ds_type', 'answer_text', 'is_impossible'])
