This dataset consists of SQuAD (Rajpurkar et al. 2016) Dev and Test sets Machine-Translated into the 6 MLQA Target Languages.

We translate paragraphs and questions from the SQuAD training set into the target language using a machine-translation system.
Prior to translating, we enclose the answer span in quotes, as was done by Lee et al. (2018). 
This makes it simple to extract the answer from the translated paragraph, as well as encouraging the translation model to map the answer into a single span

